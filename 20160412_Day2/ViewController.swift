//
//  ViewController.swift
//  20160412_Day2
//
//  Created by ChenSean on 4/12/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        //
    }
    
    override func viewWillAppear(animated: Bool) {
        //
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "VC2secondVC"){
            let str = self.myText.text;
        
            // SecondVC 繼承 UIViewController
            let vc = segue.destinationViewController as! SecondVC;
            vc.str = str!;
        }
        
    }
    
    @IBOutlet weak var myText: UITextField!
    
    @IBAction func btn_onclick(sender: AnyObject) {
        
    }
    
    @IBAction func unwind(segue: UIStoryboardSegue) {
        
    }


}

